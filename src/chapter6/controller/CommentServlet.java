package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;

@WebServlet("/comment")
public class CommentServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		String commentText = request.getParameter("text");
		int messageId = Integer.parseInt(request.getParameter("messageId"));

		List<String> errorMessages = new ArrayList<String>();
		if (!isValid(commentText, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
		} else {
			Comment comment = new Comment();
			comment.setText(commentText);

			User user = (User) session.getAttribute("loginUser");
			comment.setUserId(user.getId());
			comment.setMessageId(messageId);

			new CommentService().insert(comment);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(String comment, List<String> errorMessages) {

		if (StringUtils.isBlank(comment)) {
			errorMessages.add("入力してください");
		} else if (140 < comment.length()) {
			errorMessages.add("140文字以下で入力してください");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
